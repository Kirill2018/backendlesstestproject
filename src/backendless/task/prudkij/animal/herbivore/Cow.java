package backendless.task.prudkij.animal.herbivore;

import backendless.task.prudkij.meal.HerbivoreMeal;

public class Cow implements Herbivore {

    @Override
    public void eat(HerbivoreMeal meal) {
    }
}
