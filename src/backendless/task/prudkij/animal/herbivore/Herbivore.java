package backendless.task.prudkij.animal.herbivore;

import backendless.task.prudkij.animal.Animal;
import backendless.task.prudkij.meal.HerbivoreMeal;
import backendless.task.prudkij.meal.PredatorMeal;

public interface Herbivore extends Animal<HerbivoreMeal>, PredatorMeal {
}
