package backendless.task.prudkij.animal;

import backendless.task.prudkij.meal.Meal;

public interface Animal<T extends Meal> {
    void eat(T meal);
}
