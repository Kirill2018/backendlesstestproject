package backendless.task.prudkij.animal.predator;

import backendless.task.prudkij.animal.Animal;
import backendless.task.prudkij.meal.PredatorMeal;

public interface Predator extends Animal<PredatorMeal> {
}
