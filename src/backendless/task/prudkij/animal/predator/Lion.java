package backendless.task.prudkij.animal.predator;

import backendless.task.prudkij.meal.PredatorMeal;

public class Lion implements Predator {

    @Override
    public void eat(PredatorMeal meal) {
    }
}
